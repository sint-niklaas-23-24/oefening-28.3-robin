﻿namespace Oefening_28._3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Docent hvs = new Docent("Hans Van Soom");
            Vak basisC = new Vak("Basis C#", 11, "LOK1");
            Vak advancedC = new Vak("Advanced C#", 13, "LOK1");
            Vak web = new Vak("Web", 12, "LOK7");
            Vak git = new Vak("GIT", 2, "LOK9");

            hvs.AddVak(basisC);
            hvs.AddVak(advancedC);
            hvs.AddVak(web);
            hvs.AddVak(git);

            Console.WriteLine(hvs.ToString());

            hvs.RemoveVak(web);

            Console.WriteLine("Na het verwijderen van Web:");
            Console.WriteLine(hvs.ToString());
            Console.ReadKey();
        }
    }
}