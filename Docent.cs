﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._3
{
    internal class Docent
    {
        private string _naam;
        private List<Vak> _vakken = new List<Vak>();

        public Docent(string naam) 
        {
            Naam = naam;
        }
        public string Naam 
        { 
            get { return _naam; } 
            set { _naam = value; }
        }
        private List<Vak> Vakken
        {
            get { return _vakken;}
            set { _vakken = value;}
        }
        public void AddVak(Vak vak)
        {
            Vakken.Add(vak);
        }
        public void RemoveVak(Vak vak)
        {
            Vakken.Remove(vak);
        }
        public override string ToString()
        {
            string resultaat = Naam + " geeft de volgende vakken:" + Environment.NewLine;

            foreach (Vak vak in Vakken) 
            { 
                resultaat += vak.ToString() + Environment.NewLine;
            }

            return resultaat;
        }
    }
}
